with Ada.Text_IO; use Ada.Text_IO;
with System.Storage_Elements; use System.Storage_Elements;

package body Radio.Transmit is

  -----------
  -- State --
  -----------

  function State return Radio_State_Type is
    Radio_State : Radio_State_Type := Disabled with
      Volatile,
      Address => To_Address (State_Addr);
  begin
    return Radio_State;
  end State;

  -----------
  -- Start --
  -----------

  procedure Start (Data : in Packet_Type) is
    TX_En          : Boolean with
      Volatile,
      Address => To_Address (TX_Enable);
    Packet_Pointer : Base_Address_Type with
      Volatile,
      Address => To_Address (Packet_Pointer_Addr);
    TX_Data        : Packet_Type (Data.Length) with
      Volatile,
      Address => To_Address (TX_Address);
  begin
    Packet_Pointer := TX_Address;
    TX_Data        := Data;
    TX_En          := True;
    while State /= Disabled loop
      null;
    end loop;
  end Start;

  ------------
  -- Enable --
  ------------

  procedure Enable is
    TX_En : Boolean with
      Volatile,
      Address => To_Address (TX_Enable);
  begin
    New_Line;
    Put ("Enabling.");
    TX_En := True;
    while State /= TX_Ramp_Up loop
      Put (".");
    end loop;
  end Enable;

  -----------
  -- Start --
  -----------

  procedure Start is
    TX_Start : Boolean with
      Volatile,
      Address => To_Address (Start_Radio);
  begin
    New_Line;
    Put ("Starting.");
    while State /= TX_Idle loop
      Put (".");
    end loop;
    TX_Start := True;
  end Start;

  -------------
  -- Disable --
  -------------

  procedure Disable is
    TX_Disable : Boolean with
      Volatile,
      Address => To_Address (Disable_Radio);
  begin
    New_Line;
    Put ("Disabling.");
    while State /= TX_Idle loop
      null;
    end loop;
    TX_Disable := True;
  end Disable;

end Radio.Transmit;
