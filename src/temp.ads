with Protected_Stream; use Protected_Stream;
with System.Storage_Elements; use System.Storage_Elements;
with Ada.Interrupts.Names;

package Temp is
  
  type Temp_Celsius_Type is delta 0.25 range
    -2_147_483_648.0 / 4 .. 2_147_483_647.0 / 4
    with Size => 32;
  
  type Temp_Fahrenheit_Type is new Temp_Celsius_Type;

  subtype Temp_Stream_Array_Type is Byte_Stream_Type 
    (Stream_Size_Type'First .. Stream_Size_Type 
       ((Temp_Fahrenheit_Type'Size / Protected_Stream.Byte_Type'Size) - 1));

  function Get_Temp return Temp_Fahrenheit_Type;
  --
  -- Returns the last good temperature reading,
  -- Using this avoids the nested tasking problem
  
  procedure Init;
  --
  -- Initialize the Temperature Task
  
  protected type Temperature is
    
    procedure Set with 
      Attach_Handler => Ada.Interrupts.Names.TEMP;
    --
    -- Store the retrieved temperaturevalue, 
    -- Triggered By Interrupt
    
    entry Get (T : out Temp_Fahrenheit_Type);
    --
    -- Blocking function to Get the stored temperature value
    
  private
    Is_Set   : Boolean := False;
    Measured : Temp_Celsius_Type;
  end Temperature;
  
  T : Temperature;
  
private
  
  ---------------
  -- Registers --
  ---------------
  
  Base_Address                : constant := 16#4000_C000#;
  Task_Start_Addr             : constant := Base_Address + 16#000#;
  Task_Stop_Addr              : constant := Base_Address + 16#004#;
  Event_Data_Ready_Addr       : constant := Base_Address + 16#100#;
  Interrupt_Enable_Addr       : constant := Base_Address + 16#300#;
  Interrupt_Enable_Set_Addr   : constant := Base_Address + 16#304#;
  Interrupt_Enable_Clear_Addr : constant := Base_Address + 16#308#;
  Temperature_Addr            : constant := Base_Address + 16#508#;
 
  Measured_Temp : Temp_Celsius_Type := Temp_Celsius_Type'First;
end Temp;
-- TODO: Add Init function to set the clock to HF
