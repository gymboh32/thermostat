package Radio.Transmit is

  procedure Start (Data : in Packet_Type);
  --
  -- Transmit the Temp
  
  procedure Enable;
  --
  -- Enable the Transmit radio
  
  procedure Start; 
  --
  -- Start Transmitting the message
  
  procedure Disable;
  --
  -- Disable the Transmit radio

end Radio.Transmit;
-- TODO: Setup the interrupts?

