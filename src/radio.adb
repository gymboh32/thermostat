with Ada.Text_IO; use Ada.Text_IO;
with Clock; use Clock;

package body Radio is

  type Spare_Bit_Array_Type is array (Natural range <>) of Boolean;
  for Spare_Bit_Array_Type'Component_Size use 1;

  type Length_Type is mod 2 ** 4
    with Size => 4;

  type PCNF_0_Type is record
    Length_Field_Length : Length_Type;
    Spare_1             : Spare_Bit_Array_Type (4 .. 7);
    S0_Length           : Boolean;
    Spare_2             : Spare_Bit_Array_Type (9 ..15);
    S1_Length           : Length_Type;
    Spare_3             : Spare_Bit_Array_Type (20 .. 31);
  end record;
  for PCNF_0_Type use record
    Length_Field_Length at 0 range  0 ..  3;
    Spare_1             at 0 range  4 ..  7;
    S0_Length           at 0 range  8 ..  8;
    Spare_2             at 0 range  9 .. 15;
    S1_Length           at 0 range 16 .. 19;
    Spare_3             at 0 range 20 .. 31;
  end record;
  for PCNF_0_Type'Size use 32;

  type PCNF_1_Type is record
    Max_Length          : Byte_Type;
    Static_Length       : Byte_Type;
    Base_Address_Length : Base_Address_Length_Type;
    Spare_1             : Spare_Bit_Array_Type (19 .. 23);
    Endianness          : Boolean;
    Whitenning          : Boolean;
    Spare_2             : Spare_Bit_Array_Type (26 .. 31);
  end record;
  for PCNF_1_Type use record
    Max_Length          at 0 range  0 ..  7;
    Static_Length       at 0 range  8 .. 15;
    Base_Address_Length at 0 range 16 .. 18;
    SPare_1             at 0 range 19 .. 23;
    Endianness          at 0 range 24 .. 24;
    Whitenning          at 0 range 25 .. 25;
    Spare_2             at 0 range 26 .. 31;
  end record;
  for PCNF_1_Type'Size use 32;

  -------------------
  -- Null_Callback --
  -------------------

  procedure Null_Callback is
  begin
    null;
  end Null_Callback;

  ----------
  -- Init --
  ----------

  procedure Init is
    Mode           : Radio_Mode_Type
      with Volatile, Address => To_Address (Mode_Addr);
    PCNF_0         : PCNF_0_Type
      with Volatile, Address => To_Address (Packet_Config_0_Addr);
    PCNF_1         : PCNF_1_Type
      with Volatile, Address => To_Address (Packet_Config_1_Addr);
    CRC_Config     : Spare_Bit_Array_Type (0 .. 31)
      with Volatile, Address => To_Address (CRC_Config_Addr);
    Base_Address   : Base_Address_Type
      with Volatile, Address => To_Address (Base_0_Addr);
    Prefix_0       : array (0 .. 3) of Prefix_Address_Type
      with Volatile, Address => To_Address (Prefix_0_Addr);
    Frequency      : Frequency_Type
      with Volatile, Address => To_Address (Frequency_Addr);
    Power          : Power_Type
      with Volatile, Address => To_Address (Power_Addr);
    Receive        : array (0 .. 31) of Boolean
      with Volatile, Address => To_Address (Rx_Select_Addr);
    Transmit       : Address_Index_Type
      with Volatile, Address => To_Address (Tx_Select_Addr);
    Shortcuts      : Spare_Bit_Array_Type (0 .. 31)
      with Volatile, Address => To_Address (Shortcut_Addr);
  begin
    -- Proprietary mode
    Mode := NRF_1Mbit;

    -- Configure the packet
    PCNF_0 := (Length_Field_Length => 8,
               S0_Length           => True,
               S1_Length           => 0,
               others              => (others => False));
    PCNF_1 := (Max_Length          => Data.Data'Length,
               Static_Length       => 0,
               Base_Address_Length => 2,
               Endianness          => False,
               Whitenning          => True,
               others              => (others => False));

    -- Turn off CRC checking
    CRC_Config := (others => False);

    -- Start HF Clock for RF
    Clock.Start_HF;

    while not Clock.Is_HF_Started loop
      null;
    end loop;

    -- Configure Addresses
    Base_Address := Base_0_Address;
    Prefix_0     := (0      => Prefix_0_Address,
                     others => 0);
    Receive      := (0      => True,
                     others => False);
    Transmit     := Address_Index_Type'First;

    -- Configure Frequency and Power
    Frequency := 0;

    -- Clear Received Data
    Data.Data := (others => 0);

    -- Set shortcuts
    Shortcuts := (0      => True, -- Ready to Start
                  1      => True, -- End to Disable
                  others => False);

    -- Enable Interrupts
  end Init;

  ----------
  -- Read --
  ----------

  procedure Read is
  begin
    New_Line;
    Put_Line ("Received: ");
    For I in Data.Data'Range loop
      Put (Data.Data (I)'Img);
      New_Line;
    end loop;
  end Read;

end Radio;
