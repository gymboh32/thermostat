with System.Storage_Elements; use System.Storage_Elements;

package Protected_Stream is

  ----------------
  -- Exceptions --
  ----------------
  Stream_Is_Full  : exception;
  -- Stream is full and cannot be written to
  Stream_Is_Empty : exception;
  -- Stream is empty there is nothing to read
 
  -----------
  -- Types --
  -----------
  
  type Stream_Size_Type is mod 2 ** 32;
  
  type Byte_Type is mod 2 ** 8;
  for Byte_Type'Size use 8;
  
  type Byte_Stream_Type is 
    array (Stream_Size_Type range <>) of Byte_Type;
 
  generic 
    type Source_Type is private;
  function To_Byte_Stream (Source : Source_Type) 
                           return Byte_Stream_Type;
  --
  -- Generic function to return a Byte_Stream_Type
  
  generic
    type Target_Type is private;
  function From_Byte_Stream (Byte_Array : Byte_Stream_Type)
                             return Target_Type;
  --
  -- Generic function to return a Target
  protected type Stream_Type is
    
    procedure Initialize (Address : Integer_Address;
                          Length  : Stream_Size_Type);
    --
    -- Initialize a new Stream
    
    procedure Read (Data : out Byte_Stream_Type);
    --
    -- Read Data from Stream
    
    procedure Write (Data : in Byte_Stream_Type);
    --
    -- Write Data to Stream
    
  private
    Stream_Address : System.Address   := To_Address (16#2000_0000#);
    Stream_Length  : Stream_Size_Type := Stream_Size_Type'Last;
    Stream_Last    : Stream_Size_Type := Stream_Size_Type'First;
  end Stream_Type;
  
  RAM_0 : Stream_Type;
  RAM_1 : Stream_Type;

end Protected_Stream;
