with Interfaces; use Interfaces;
with System;

package body Clock is

  -----------
  -- Start --
  -----------

  procedure Start (Clock : in Clock_Type) is
    Start_Word : Interfaces.Unsigned_32 := 0;
    for Start_Word'Address use System'To_Address (case Clock is
                                                    when High => HF_Start,
                                                    when Low  => LF_Start);
    pragma Volatile (Start_Word);
  begin
    Start_Word := 1;
  end Start;

  ----------
  -- Stop --
  ----------

  procedure Stop (Clock : in Clock_Type) is
    Stop_Word : Interfaces.Unsigned_32 := 0;
    for Stop_Word'Address use System'To_Address (case Clock is
                                                   when High => HF_Stop,
                                                   when Low  => LF_Stop);
    pragma Volatile (Stop_Word);
  begin
    Stop_Word := 1;
  end Stop;

  ----------------
  -- Is_Started --
  ----------------

  function Is_Started (Clock : Clock_Type) return Boolean is
    type Bit_Array_Type is array (0 .. 31) of Boolean;
    for Bit_Array_Type'Component_Size use 1;
    for Bit_Array_Type'Size use 32;
        Started_Word : Bit_Array_Type;
    for Started_Word'Address use System'To_Address (case Clock is
                                                      when High => HF_Clock_Stat,
                                                      when Low  => LF_Started);
  begin
    return Started_Word (16);
--        (case Started_Word is
--                when 1      => True,
--                when others => False);
  end Is_Started;

  --------------
  -- Start_LF --
  --------------

  procedure Start_LF is
  begin
    Start (Low);
  end Start_LF;

  -------------
  -- Stop_LF --
  -------------

  procedure Stop_LF is
  begin
    Stop (Low);
  end Stop_LF;

  -------------------
  -- Is_LF_Started --
  -------------------

  function Is_LF_Started return Boolean is
  begin
    return Is_Started (Low);
  end Is_LF_Started;

  --------------
  -- Start_HF --
  --------------

  procedure Start_HF is
    HF_Clock_Src : Boolean with
      Volatile,
      Size    => 32,
      Address => To_Address (HF_Clock_Stat);
  begin
    HF_Clock_Src := False; -- RC => 0, Xtal => 1
    Start (High);
  end Start_HF;

  -------------
  -- Stop_HF --
  -------------

  procedure Stop_HF is
  begin
    Stop (High);
  end Stop_HF;

  -------------------
  -- Is_HF_Started --
  -------------------

  function Is_HF_Started return Boolean is
  begin
    return Is_Started (High);
  end Is_HF_Started;

end Clock;
