with Ada.Text_IO; use Ada.Text_IO;
with System.Storage_Elements; use System.Storage_Elements;

package body Radio.Receive is

  -----------
  -- State --
  -----------

  function State return Radio_State_Type is
    Radio_State : Radio_State_Type := Disabled with
      Volatile,
      Address => To_Address (State_Addr);
  begin
    return Radio_State;
  end State;

  -----------
  -- Start --
  -----------

  procedure Start (Callback : Callback_Type := Null_Callback'Access) is
    RX_En : Boolean with
      Volatile,
      Address => To_Address (RX_Enable);
    Packet_Pointer : Base_Address_Type with
      Volatile,
      Address => To_Address (Packet_Pointer_Addr);
  begin
    Packet_Pointer := RX_Address;
    RX_En := True;
    while State /= Disabled loop
      null;
    end loop;
    Callback.all;
  end Start;

  ------------
  -- Enable --
  ------------

  procedure Enable is
    RX_En : Boolean with
      Volatile,
      Address => To_Address (RX_Enable);
  begin
    New_Line;
    Put ("Enabling.");
    RX_En := True;
    while State /= RX_Ramp_Up loop
      Put (".");
    end loop;
  end Enable;

  -----------
  -- Start --
  -----------

  procedure Start is
    RX_Start : Boolean with
      Volatile,
      Address => To_Address (Start_Radio);
  begin
    New_Line;
    Put ("Starting...");
    while State /= RX_Idle loop
      Put (".");
    end loop;
    RX_Start := True;
  end Start;

  -------------
  -- Disable --
  -------------

  procedure Disable is
    RX_Disable : Boolean with
      Volatile,
      Address => To_Address (Disable_Radio);
  begin
    New_Line;
    Put ("Disabling.");
    while State /= RX_Idle loop
      null;
    end loop;
    RX_Disable := True;
  end Disable;

end Radio.Receive;
