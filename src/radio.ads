with System.Storage_Elements; use System.Storage_Elements;

package Radio is
  
  type Shortcut_Type is 
    (Ready_To_Start_Radio,  End_To_Disable_Radio, 
     Disabled_To_TX_Enable, Disabled_To_RX_Enable, 
     Address_To_RSSI_Start, End_To_Start_Radio,
     Address_To_BC_Start,   Address_To_BC_Stop,
     Spare,                 Disabled_To_RSSI_Stop);

  type Frequency_Type is range 0 .. 100 -- 2400MHz .. 2500MHz
    with Size => 7;
  
  type Power_Type is 
    (Zero,       Pos_4_DBm, 
     Neg_30_DBm, Neg_20_DBm,
     Neg_16_DBm, Neg_12_DBm,
     Neg_8_DBm,  Neg_4_DBm)
    with Size => 8;
  
  type Radio_Mode_Type is 
    (NRF_1Mbit,   NRF_2Mbit, 
     NRF_250Kbit, BLE_1Mbit) 
    with Size => 3;
  
  type Radio_State_Type is
    (Disabled, 
     RX_Ramp_Up, RX_Idle, RX, RX_Disable, 
     TX_Ramp_Up, TX_Idle, TX, TX_Disable)
    with Size => 4;
  
  type Base_Address_Length_Type is range 2 .. 4
    with Size => 3;
  
  type Byte_Length_Type is mod 2**8 with Size => 8;
  type Bit_Length_Type  is mod 2**4 with Size => 4; 

  type Byte_Type is mod 2 ** 8
    with Size => 8;
  
  type Byte_Array_Type is
    array (Bit_Length_Type range <>) of Byte_Type;
  
  type Message_ID_Type is mod 2**8;
  -- TODO: This should be an enumeration of the messages
  -- TODO: Or the hash id of the adapter that gets downloaded 
  --       on synchronization
  
  type Packet_Type (Length : Bit_Length_Type) is record
    Id   : Message_ID_Type;
    Data : Byte_Array_Type (Bit_Length_Type'First .. Length);
  end record;
  
  type Base_Address_Type is mod 2 ** 32 with Size => 32;
  type Prefix_Address_Type is mod 2 ** 8 with Size => 8;
  type Address_Index_Type is mod 2 ** 3 with Size => 3;

  TX_Address : constant := 16#2000_1000#;
  RX_Address : constant := 16#2000_0000#;
  
  type Callback_Type is access procedure;
  --
  -- Callback used to react to a received message

  procedure Null_Callback;
  --
  -- Null Procedure, default Callback

  procedure Init;
  --
  -- Initialize the Radio
  
  procedure Read;
  --
  -- Print the values from last message received
  
private

  for Power_Type use 
    (Zero       => 16#00#, Pos_4_DBm  => 16#04#, 
     Neg_30_DBm => 16#D8#, Neg_20_DBm => 16#EC#,
     Neg_16_DBm => 16#F0#, Neg_12_DBm => 16#F4#, 
     Neg_8_DBm  => 16#F8#, Neg_4_DBm  => 16#FC#);
  
  for Radio_State_Type use
    (Disabled   => 0,
     RX_Ramp_Up => 1, RX_Idle => 2,  RX => 3,  RX_Disable => 4,
     TX_Ramp_Up => 9, TX_Idle => 10, TX => 11, TX_Disable => 12);
  
  Base_Address : constant := 16#4000_1000#;
  
  -----------
  -- Tasks --
  -----------

  TX_Enable     : constant := Base_Address + 16#000#; -- Transmit
  RX_Enable     : constant := Base_Address + 16#004#; -- Receive
  Start_Radio   : constant := Base_Address + 16#008#;
  Stop_Radio    : constant := Base_Address + 16#00C#;
  Disable_Radio : constant := Base_Address + 16#010#;
  RSSI_Start    : constant := Base_Address + 16#014#; -- Received Signal Strength Indicator
  RSSI_Stop     : constant := Base_Address + 16#018#; -- Received Signal Strength Inidicator
  BC_Start      : constant := Base_Address + 16#01C#; -- Bit Counter
  BC_Stop       : constant := Base_Address + 16#020#; -- Bit_Counter
  
  ------------
  -- Events --
  ------------
  
  Radio_Ready    : constant := Base_Address + 16#100#;
  Address_RX_TX  : constant := Base_Address + 16#104#;
  Payload        : constant := Base_Address + 16#108#;
  End_Radio      : constant := Base_Address + 16#10C#;
  Disabled_Radio : constant := Base_Address + 16#110#;
  Device_Match   : constant := Base_Address + 16#114#;
  Device_Miss    : constant := Base_Address + 16#118#;
  RSSI_End       : constant := Base_Address + 16#11C#;
  BC_Match       : constant := Base_Address + 16#128#;
  
  ---------------
  -- Registers --
  ---------------
  
  Shortcut_Addr             : constant := Base_Address + 16#200#;
  Set_Interrupts_Addr       : constant := Base_Address + 16#304#;
  Clear_Interrupts_Addr     : constant := Base_Address + 16#308#;
  CRC_Status_Addr           : constant := Base_Address + 16#400#;
  Received_Match_Addr       : constant := Base_Address + 16#408#;
  Received_CRC_Addr         : constant := Base_Address + 16#40C#;
  Device_Address_Index_Addr : constant := Base_Address + 16#410#;  
  Packet_Pointer_Addr       : constant := Base_Address + 16#504#;
  Frequency_Addr            : constant := Base_Address + 16#508#;
  TX_Power_Addr             : constant := Base_Address + 16#50C#;
  Mode_Addr                 : constant := Base_Address + 16#510#;
  Packet_Config_0_Addr      : constant := Base_Address + 16#514#;
  Packet_Config_1_Addr      : constant := Base_Address + 16#518#;
  Base_0_Addr               : constant := Base_Address + 16#51C#;
  Base_1_Addr               : constant := Base_Address + 16#520#;
  Prefix_0_Addr             : constant := Base_Address + 16#524#; -- Prefix Addresses 0 .. 3
  Prefix_1_Addr             : constant := Base_Address + 16#528#; -- Prefix Addresses 4 .. 7
  Tx_Select_Addr            : constant := Base_Address + 16#52C#;
  Rx_Select_Addr            : constant := Base_Address + 16#530#;
  CRC_Config_Addr           : constant := Base_Address + 16#534#;
  CRC_Poly_Addr             : constant := Base_Address + 16#538#;
  CRC_Init_Addr             : constant := Base_Address + 16#53C#;
  Test_Enable_Addr          : constant := Base_Address + 16#540#;
  Interframe_Spacing_Addr   : constant := Base_Address + 16#544#; -- TIFS
  RSSI_Sample_Addr          : constant := Base_Address + 16#548#;
  State_Addr                : constant := Base_Address + 16#550#;
  Data_Whitening_Addr       : constant := Base_Address + 16#554#;
  Bit_Counter_Compare_Addr  : constant := Base_Address + 16#560#;
  Power_Addr                : constant := Base_Address + 16#FFC#;
  
  Base_0_Address   : constant Base_Address_Type   := 16#7542_6974#; -- ASCII for "uBit"
  Prefix_0_Address : constant Prefix_Address_Type := 16#A5#; -- TODO: Make Unique
  -- This is the group_id used in lancaster to identify microbits in a network
  Data : Packet_Type (Bit_Length_Type'Last)
    with Volatile, Address => To_Address (16#2000_0000#);

end Radio;
