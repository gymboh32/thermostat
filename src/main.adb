with Ada.Text_IO; use Ada.Text_IO;
with Temp; use Temp;

procedure Main is
  Temperature : Temp_Fahrenheit_Type :=
                  Temp_Fahrenheit_Type'First;
begin
  loop
    T.Get (Temperature);
    Put ("...");
    Put_Line ("Temp: " & Integer (Temperature)'Img);
  end loop;
end Main;
