with Ada.Text_IO; use Ada.Text_IO;
with Unchecked_Conversion;

package body Temp is

  --------------
  -- Get_Temp --
  --------------

  function Get_Temp return Temp_Fahrenheit_Type is
  begin
    return Measured_Temp * 1.8 + 32.0;
  end Get_Temp;

  ----------
  -- Init --
  ----------

  procedure Init is
    Start_Task       : Boolean with
      Volatile,
      Address => To_Address (Task_Start_Addr);
    Enable_Interrupt : Boolean with
      Volatile,
      Address => To_Address (Interrupt_Enable_Set_Addr);
  begin
    Enable_Interrupt := True;
    Start_Task       := True;
  end Init;

  ---------------
  -- Protected --
  ---------------

  protected body Temperature is

    ---------
    -- Set --
    ---------

    procedure Set is
      Stop_Task       : Boolean with
        Volatile,
        Address => To_Address (Task_Stop_Addr);
      Raw_Temp        : Integer := 0 with
        Volatile,
        Address => To_Address (Temperature_Addr);
      Clear_Interrupt : Boolean with
        Volatile,
        Address => To_Address (Interrupt_Enable_Clear_Addr);
    begin
      -- Clear the interrupt event
      Clear_Interrupt := True;

      -- Stop the Temp reading task
      Stop_Task := True;

      -- Retrieve the Temperature value
      -- 0.25 LSB, 2s Complement
      Measured := Temp_Celsius_Type (Float (Raw_Temp) * 0.25);

      Measured_Temp := Measured;

      -- Reset the Flag to unblock the Get task
      Is_Set := True;
    end Set;

    ---------
    -- Get --
    ---------

    entry Get (T : out Temp_Fahrenheit_Type)
      when Is_Set is
    begin
      -- Retrieve and convert the temperature value to Fahrenheit
      T := Measured * 1.8 + 32.0;

      -- Reset the Flag to block this task until another read event is
      -- Initiated
      Is_Set := False;
    end Get;
  end Temperature;

end Temp;
