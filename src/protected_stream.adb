with Unchecked_Conversion;

package body Protected_Stream is

  --------------------
  -- To_Byte_Stream --
  --------------------

  function To_Byte_Stream (Source : Source_Type)
                           return Byte_Stream_Type is
    subtype Source_Stream_Type is Byte_Stream_Type
      (Stream_Size_Type'First ..
         Stream_Size_Type (Source_Type'Size / Byte_Type'Size - 1));
    function Convert is new Unchecked_Conversion
      (Source => Source_Type,
       Target => Source_Stream_Type);
  begin
    return Convert (Source);
  end To_Byte_Stream;

  ----------------------
  -- From_Byte_Stream --
  ----------------------

  function From_Byte_Stream (Byte_Array : Byte_Stream_Type)
                           return Target_Type is
    subtype Target_Stream_Type is Byte_Stream_Type
      (Stream_Size_Type'First ..
         Stream_Size_Type (Target_Type'Size / Byte_Type'Size - 1));
    function Convert is new Unchecked_Conversion
      (Source => Target_Stream_Type,
       Target => Target_Type);
  begin
    return Convert (Byte_Array);
  end From_Byte_Stream;

  -----------------
  -- Stream_Type --
  -----------------

  protected body Stream_Type is

    ----------------
    -- Initialize --
    ----------------

    procedure Initialize (Address : Integer_Address;
                          Length  : Stream_Size_Type) is
    begin
      Stream_Address := System'To_Address (Address);
      Stream_Length  := Length;
    end Initialize;

    ----------
    -- Read --
    ----------

    procedure Read (Data : out Byte_Stream_Type) is
      Stream_Data : Byte_Stream_Type
        (Stream_Size_Type'First .. Stream_Length)
        with Volatile, Address => Stream_Address;
      Index : Stream_Size_Type := Stream_Size_Type'First;
    begin
--        if Stream_Last  Stream_Length then
--          raise Stream_Is_Empty with
--            "Stream is empty - Cannot Read";
--        end if;

      Data := Stream_Data (Stream_Size_Type'First .. Data'Last);

      for I in Stream_Size_Type'First .. Stream_Length loop
        Index := Stream_Size_Type'Min (Stream_Length,
                                       I + Data'Last + 1);
        Stream_Data (I) := Stream_Data (Index);
      end loop;

      Stream_Last := Stream_Last - Data'Last - 1;
    end Read;

    -----------
    -- Write --
    -----------

    procedure Write (Data : in Byte_Stream_Type) is
      Stream_Data : Byte_Stream_Type
        (Stream_Size_Type'First .. Stream_Length)
        with Volatile, Address => Stream_Address;
    begin
      if Stream_Last >= Stream_Size_Type'Last - Data'Last then
        raise Stream_Is_Full with
          "Stream is full - Cannot Write";
      end if;

      Stream_Data (Stream_Last .. Stream_Last + Data'Last) := Data;
      Stream_Last := Stream_Last + Data'Last + 1;
    end Write;

  end Stream_Type;

begin
  RAM_0.Initialize (16#2000_0000#,
                    16#0FFF#);
  RAM_1.Initialize (16#2000_1000#,
                    16#0FFF#);

end Protected_Stream;
