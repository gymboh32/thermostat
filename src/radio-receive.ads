package Radio.Receive is
  
  procedure Start (Callback : Callback_Type := Null_Callback'Access);
  --
  -- Start Receiving messages, react with callback
  
  procedure Enable;
  --
  -- Enable the Receive radio
  
  procedure Start; 
  --
  -- Start Receiving the message
  
  procedure Disable;
  --
  -- Disable the Receive radio

end Radio.Receive;
-- TODO: Enable Interrupt driven Receive?
