with Interfaces; use Interfaces;
with System.Storage_Elements; use System.Storage_Elements;

package Clock is

  type Clock_Type is (High, Low);

  -------------
  -- Generic --
  -------------
   
  procedure Start (Clock : in Clock_Type);
  --
  -- Starts the specified Clock
   
  procedure Stop (Clock : in Clock_Type);
  --
  -- Stops the specified Clock
   
  function Is_Started (Clock : Clock_Type) return Boolean;
  --
  -- Returns True if and only if the specified clock is started.
  -- Otherwise, returns False.
   
  -------------------
  -- Low_Frequency --
  -------------------
   
  procedure Start_LF;
  --
  -- Starts the Low Frequency Clock
   
  procedure Stop_LF;
  --
  -- Stops the Low Frequency Clock
   
  function Is_LF_Started return Boolean;
  --
  -- Returns True if and only if the Low Frequency Clock is started.
  -- Otherwise, returns False.
   
  --------------------
  -- High_Frequency --
  --------------------
   
  procedure Start_HF;
  --
  -- Starts the High Frequency Clock
   
  procedure Stop_HF;
  --
  -- Stops the High Frequency Clock
   
  function Is_HF_Started return Boolean;
  --
  -- Returns True if and only if the High Frequency Clock is started.
  -- Otherwise, returns False,
  
private
  
  Base_Address : constant := 16#4000_0000#;
  
  -----------
  -- Tasks --
  -----------
   
  HF_Start : constant := Base_Address + 16#0000#;
  HF_Stop  : constant := Base_Address + 16#0004#;
  LF_Start : constant := Base_Address + 16#0008#;
  LF_Stop  : constant := Base_Address + 16#000C#;
   
  ------------
  -- Events --
  ------------
   
  HF_Started : constant := Base_Address + 16#0100#;
  LF_Started : constant := Base_Address + 16#0104#;
   
  ---------------
  -- Registers --
  ---------------
   
  Interrupt_Enable : constant := Base_Address + 16#0304#;
  Interrupt_Clear  : constant := Base_Address + 16#0308#;
  HF_Clock_Stat    : constant := Base_Address + 16#040C#;
  
end Clock;
