with Ada.Text_IO; use Ada.Text_IO;
with Interfaces; use Interfaces;
with Ada.Interrupts.Names; use Ada.Interrupts;

package body Timer is

  -----------
  -- Start --
  -----------

  procedure Start (Timer : Timer_Type := Timer_0) is
    Start_Timer      : Boolean with
      Volatile,
      Address => To_Address (Timer'Enum_Rep + Start_Address);
    Enable_Interrupt : Interfaces.Unsigned_32 with
      Volatile,
      Address => To_Address (Timer'Enum_Rep + Interrupt_Enable_Set);
  begin
    -- Enable the Interrupt
    Enable_Interrupt := 2#00000000000000010000000000000000#;
    -- Start the Timer Task
    Start_Timer      := True;
  end Start;

  ---------------
  -- Task_Type --
  ---------------

  protected body Timer_Task is

    ----------
    -- Init --
    ----------

    entry Init (Callback : in Callback_Type := Null_Callback'Access;
                Compare  : in Compare_Value_Type;
                Prescale : in Prescaler_Type := 4)
      when Is_Not_Initialized is
      Start_Timer      : Boolean with
        Volatile,
        Address => To_Address (Timer'Enum_Rep + Start_Address);
      Enable_Interrupt : Interfaces.Unsigned_32 with
        Volatile,
        Address => To_Address (Timer'Enum_Rep + Interrupt_Enable_Set);
      Mode_Word        : Mode_Type with
        Volatile,
        Address => To_Address (Timer'Enum_Rep + Mode_Address);
      Bitmode_Word     : Bitmode_Type with
        Volatile,
        Address => To_Address (Timer'Enum_Rep + Bitmode);
      Prescaler_Word   : Prescaler_Type with
        Volatile,
        Address => To_Address (Timer'Enum_Rep + Prescaler);
      Compare_Word     : Compare_Value_Type with
        Volatile,
        Address => To_Address (Timer'Enum_Rep + CC_0'Enum_Rep);
    begin
      Put_Line ("Initialize");
      -- Save the Callback procedure
      Timer_Callback   := Callback;
      -- Set to Timer mode
      Mode_Word        := Time;
      -- 32 Bit Compare word
      Bitmode_Word     := Bit_32;
      -- Set the Prescale, default to 1Hz
      Prescaler_Word   := Prescale;
      -- Value to be Compared
      Compare_Word     := Compare;
      -- Set Flag to Lock Init
      Is_Not_Initialized := False;
      -- Enable the Interrupt
      Enable_Interrupt := 2#00000000000000010000000000000000#;
      -- Start the Timer Task
      Start_Timer      := True;
    end Init;

    ----------
    -- Stop --
    ----------

    procedure Stop is
      Shutdown_Word   : Boolean with
        Volatile,
        Address => To_Address (Timer'Enum_Rep + Shutdown_Address);
      Clear_Interrupt : Interfaces.Unsigned_32 with
        Volatile,
        Address => To_Address (Timer'Enum_Rep + Interrupt_Enable_Clear);
      Clear_Compare   : Boolean with
        Volatile,
        Address => To_Address (Timer'Enum_Rep + Clear_Address);
    begin
      -- Clear the Interrupt Event
      Clear_Interrupt := 2#00000000000000010000000000000000#;

      -- Clear the Compare Register
      Clear_Compare   := True;

      -- Shutdown the Timer Task
      Shutdown_Word   := True;

      -- Spawn the Callback procedure
      Timer_Callback.all;

      -- Reset Initialized Flag
      Is_Not_Initialized := True;
    end Stop;

  end Timer_Task;

  -------------------
  -- Null_Callback --
  -------------------

  procedure Null_Callback is
  begin
    null;
  end Null_Callback;

end Timer;
