with System.Storage_Elements; use System.Storage_Elements;
with Interfaces; use Interfaces;
with Ada.Interrupts.Names; use Ada.Interrupts;

package Timer is

  type Timer_Type is (Timer_0, Timer_1, Timer_2);
  for Timer_Type use (Timer_0 => 16#4000_8000#,
                      Timer_1 => 16#4000_9000#,
                      Timer_2 => 16#4000_A000#);


  type Mode_Type is (Time, Counter) with 
    Size => 1;
  
  type Bitmode_Type is (Bit_16, Bit_08, Bit_24, Bit_32) with
    Size => 2;
  
  type Prescaler_Type is range 0 .. 9 with
    Size => 4;
  
  type Capture_Type is (CC_0, CC_1, CC_2, CC_3);
  
  type Compare_Value_Type is new Interfaces.Integer_32;
  
  subtype Compare_Value_8_Type is Compare_Value_Type range 0 .. 2**8;

  subtype Compare_Value_16_Type is Compare_Value_Type range 0 .. 2**16;
  
  subtype Compare_Value_24_Type is Compare_Value_Type range 0 .. 2**24;
  
  type Callback_Type is access procedure;
  --
  -- Callback used to react to a timer completion
  
  procedure Null_Callback;
  --
  -- Null Procedure, default Callback

  procedure Start (Timer : Timer_Type := Timer_0);
  --
  -- Start the Specified Timer

  protected type Timer_Task (Timer : Timer_Type := Timer_0) is
    entry Init (Callback : in Callback_Type  := Null_Callback'Access;
                Compare  : in Compare_Value_Type;
                Prescale : in Prescaler_Type := 4);
    -- 
    -- Initialize a new timer with specified settings
    
    procedure Stop with
      Attach_Handler => (case Timer is
                           when Timer_0 => Names.TIMER0,
                           when Timer_1 => Names.TIMER1,
                           when Timer_2 => Names.TIMER2);
    --
    -- Interrupt handler, will shutdown timer task and call callback
    
    private
      Is_Not_Initialized : Boolean := True;
  end Timer_Task;
  
  T_0 : Timer_Task (Timer_0);
  T_1 : Timer_Task (Timer_1);
  T_2 : Timer_Task (Timer_2);
  
private
  Timer_Callback : Callback_Type;
  
  -----------
  -- Tasks --
  -----------
  
  Start_Address          : constant := 16#0000#;
  Stop_Address           : constant := 16#0004#;
  Count_Address          : constant := 16#0008#;
  Clear_Address          : constant := 16#000C#;
  Shutdown_Address       : constant := 16#0010#;
  Capture_0_Task_Address : constant := 16#0040#;
  Capture_1_Task_Address : constant := 16#0044#;
  Capture_2_Task_Address : constant := 16#0048#;
  Capture_3_Task_Address : constant := 16#004C#;
  
  ------------
  -- Events --
  ------------

  Compare_0_Event_Address : constant := 16#0140#;
  Compare_1_Event_Address : constant := 16#0144#;
  Compare_2_Event_Address : constant := 16#0148#;
  Compare_3_Event_Address : constant := 16#014C#;
  
  ---------------
  -- Registers --
  ---------------
  
  Shorts                 : constant := 16#0200#;
  Interrupt_Enable_Set   : constant := 16#0304#;
  Interrupt_Enable_Clear : constant := 16#0308#;
  Mode_Address           : constant := 16#0504#;
  Bitmode                : constant := 16#0508#;
  Prescaler              : constant := 16#0510#;
  Capture_Compare_0      : constant := 16#0540#;
  Capture_Compare_1      : constant := 16#0544#;
  Capture_Compare_2      : constant := 16#0548#;
  Capture_Compare_3      : constant := 16#054C#;

  for Capture_Type use (CC_0 => Capture_Compare_0,
                        CC_1 => Capture_Compare_1,
                        CC_2 => Capture_Compare_2,
                        CC_3 => Capture_Compare_3);
  
end Timer;
-- TODO: Move the Start Procedure outside the protected type to prevent 
--       Exceptions on short timers
-- TODO: Refactor this so it makes more sense?
-- TODO: Protect Timer from being started or initialized if already active
-- TODO: Fix the Interrupt Enable to be specific to the timer being launched


